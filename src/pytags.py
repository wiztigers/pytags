#!/usr/bin/python 

import files
import sys, os

from argparse import ArgumentParser

def _createArgumentParser():
	parser = ArgumentParser(description="Tags based (sorta) filesystem")
	parser.add_argument('-s','--show', action='store_true', 
			help="Show file tags")
	parser.add_argument('-a','--add', 
			help="Add tag to file. Do nothing if file already has tag. A tag wich is to be deleted will not be added.")
	parser.add_argument('-d','--delete', 
			help="Delete tag from file. Do nothing if file does not have tag.")
	parser.add_argument('-r','--reset', action='store_true', 
			help="Delete all tags from file. Do nothing if file does not have any tag. WARNING: This takes precedence over any other command.")
	parser.add_argument('path', help="Parameter file")
	return parser

class Arguments(object):
	def __init__(self, path, ladd=None, ldel=None, reset=False, show=False):
		self.path = path
		self.add = ladd or []
		self.delete = ldel or []
		self.reset = reset
		self.show = show

	def __repr__(self):
		return 'path=%s add=%s del=%s reset=%s show=%s'%(self.path, self.add, self.delete, self.reset, self.show)

	@classmethod
	def fromArgs(class_, args):
		add = None
		delete = None
		if not args.reset:
			if args.delete is not None:
				delete = [x for x in args.delete.split(',')]
			if args.add is not None:
				add = [x for x in args.add.split(',')]
				if args.delete is not None:
					add = [x for x in add if x not in delete]
		return class_(args.path,
				ladd=add,
				ldel=delete,
				reset=args.reset,
				show=args.show)

class Runner(object):

	def run(self, args):
		if args.reset:
			files.reset(args.path)
		for tag in args.add:
			files.add(args.path, tag)
		for tag in args.delete:
			files.suppr(args.path, tag)
		if args.show:
			files.show(args.path)



if __name__ == '__main__':

	parser = _createArgumentParser()
	args = Arguments.fromArgs(parser.parse_args())
	Runner().run(args)

