import xattr, string

XATTR_TAGS	= "user.tags"

# === UTILS =============================

def insert(tags, tag):
	""" Inserts a string in a list of strings.
	If the string contains some ',', it will be split in as many strings to insert.

	Keyword arguments:
	tags -- string list
	tag  -- string to insert.
	"""
	if string.find(tag, ',') == -1:
		if tags.count(tag) == 0:
			tags.append(tag)
	else:
		for t in string.split(tag, ','):
			insert(tags, t)
	tags.sort()

def remove(tags, tag):
	""" Removes a string from a string list.
	If the string contains some ',', it will be split in as many strings to remove.

	Keyword arguments:
	tags -- string list
	tag  -- string to remove
	"""
	if string.find(tag, ',') == -1:
		try:
			tags.remove(tag)
		except ValueError:
			# tag not in tags, do nothing
			return
	else:
		for t in string.split(tag, ','):
			remove(tags, t)

# === COMMANDS ===========================

def get(path, key):
	""" Returns the value of a given xattr for a given file as a string list.
	',' in the xattr value is the separator between elements of the list.

	Keyword arguments:
	path -- path to file
	key  -- xattribute to be removed (default XATTR_TAGS)
	"""
	try:
		return string.split(xattr.get(path, key), ',')
	except IOError:
		return []
	
# ---------------------------------------

def show(path):
	""" Prints all xattrs of a given file. """
#	attrs = xattr.get_all(path)
#	for key in attrs:
#		print('%s=%s'%(key[0], key[1]))
	attrs = get(path, XATTR_TAGS)
	print('%s: %s'%(path, attrs))

# ---------------------------------------

def add(path, tag, key=XATTR_TAGS):
	""" Adds a given tag to a given file.

	Keyword arguments:
	path -- path to file
	tag  -- tag to add
	key  -- xattribute to be modified (default XATTR_TAGS)
	"""
	tags = get(path, key)
	insert(tags, tag)
	xattr.set(path, key, ",".join(tags))
	return
	
# ---------------------------------------

def reset(path, key=XATTR_TAGS):
	""" Removes a given xattr from a given file.

	Keyword arguments:
	path -- path to file
	key  -- xattribute to be removed (default XATTR_TAGS)
	"""
	try:
		xattr.remove(path, key)
	except IOError:
		# key is no xattr, do nothing
		pass

# ---------------------------------------

def suppr(path, tag, key=XATTR_TAGS):
	""" Removes a tag

	Keyword arguments
	path -- path to file
	tag  -- tag to be removed
	key  -- xattribute to modify
	"""
	tags = get(path, key)
	remove(tags, tag)
	if tags == []:
		reset(path, key)
	else:
		xattr.set(path, key, ",".join(tags))
	return

